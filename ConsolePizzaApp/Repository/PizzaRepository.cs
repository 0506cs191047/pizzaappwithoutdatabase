﻿using ConsolePizzaApp.Model;

namespace ConsolePizzaApp.Repository
{
    internal class PizzaRepository : IPizzaRepository
    {
        public int Totalprice = default;
        List<Pizza> pizzaList;
        List<Cart> pizzapriceList;
        List<Cart> carts;
        public PizzaRepository()
        {
            pizzaList = new List<Pizza>()
            {
                new Pizza(){Id=1, Name="User1", Phone_number="9198765675", City="Bhopal", Pincode=462001},
                new Pizza(){Id=2, Name="User2", Phone_number="9234736476", City="Kokta", Pincode=462001}
            };
            pizzapriceList = new List<Cart>()
            {
                new Cart(){PizzaId=1, PizzaName = "MargheritaPizza", Price= 99},
                new Cart(){PizzaId=2, PizzaName = "Cheese n Corn Pizza", Price= 169},
                new Cart(){PizzaId=3, PizzaName = "Cheese n Tomato Pizza", Price= 499},
                new Cart(){PizzaId=4, PizzaName = "Double Cheese Margherita Pizza", Price= 189},
                new Cart(){PizzaId=5, PizzaName = "Fresh Veggie Pizza", Price= 289},
                new Cart(){PizzaId=6, PizzaName = "Farmhouse Pizza", Price= 699},
                new Cart(){PizzaId=7, PizzaName = "Peppy Paneer Pizza", Price= 299},
                new Cart(){PizzaId=8, PizzaName = "Veggie Paradise Pizza", Price= 599}

            };
            carts = new List<Cart>() { };
        }
        // Get Menu
        public List<Cart> Pizzalist()
        {
            return pizzapriceList;
        }
        //Get User
        public List<Pizza> GetAllUsers()
        {
            return pizzaList;
        }

        //Add user
        public bool RegisterUser(Pizza pizza)
        {
            var userExists = GetUserByName(pizza.Name);
            if (userExists == null)
            {
                pizzaList.Add(pizza); //Add the Data
                return true;
            }
            else
            {
                return false;
            }
        }
        // Delete User
        public bool DeleteUser(Pizza pizza1)
        {
            var Exists = GetUserByName(pizza1.Name);
            if (Exists != null)
            {
                return pizzaList.Remove((Pizza)Exists);
                return true;
            }
            else
            {
                return false;
            }
        }
        //Add User
        private object GetUserByName(string name)
        {
            return pizzaList.Find(u => u.Name == name);
        }
        // Add to Cart Process
        public List<Cart> Pizzalist2()
        {
            return carts;
        }
        // Add to Carts 
        public bool AddToCart(int id)
        {
            foreach(Cart pizza in pizzapriceList)
            {
                if(pizza.PizzaId== id)
                {
                    carts.Add(pizza);
                    Totalprice += pizza.Price;
                    Console.WriteLine($"Total Price::{Totalprice}");
                    return true;
                }
            }
            return false;

        }
        // Sorted List
        public bool Sortedbylist(Cart cart)
        {
            return true;
        }

        public bool DeletePizzaToCart(int id2)
        {
            foreach (Cart pizza in pizzapriceList)
            {
                if (pizza.PizzaId == id2)
                {
                    carts.Remove(pizza);
                    Totalprice -= pizza.Price;
                    Console.WriteLine($"Total Price::{Totalprice}");
                    return true;
                }
            }
            return false;
        }
    }

        
    
}
