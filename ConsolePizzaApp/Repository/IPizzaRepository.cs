﻿using ConsolePizzaApp.Model;

namespace ConsolePizzaApp.Repository
{
    internal interface IPizzaRepository
    {
        List<Cart> Pizzalist();
        List<Pizza> GetAllUsers();
        bool RegisterUser(Pizza pizza);
        bool AddToCart(int id);
        bool DeleteUser(Pizza pizza1);
        bool Sortedbylist(Cart cart);
        List<Cart> Pizzalist2();
        bool DeletePizzaToCart(int id2);
    }
}
