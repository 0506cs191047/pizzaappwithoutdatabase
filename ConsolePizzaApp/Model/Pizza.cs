﻿namespace ConsolePizzaApp.Model
{
    internal class Pizza
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone_number { get; set; }
        //public string Address { get; set; }
        public string City { get; set; }
        public int Pincode { get; set; }

        public override string ToString()
        {
            return $"Id::{Id}\t Name::{Name}\t Phone_number::{Phone_number}\t City::{City}\t Pincode::{Pincode}";
        }
    }
    internal class Cart
    {
        public int PizzaId { get; set; }
        public string PizzaName { get; set; }
        public int Price { get; set; }
        public override string ToString()
        {
            return $"PizzaId::{PizzaId}::-\tPizzaName::{PizzaName}::--\t Price::{Price}";
        }
    }
}
